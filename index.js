const request = require("request");
const fs = require('fs');
const path = require('path');
const mkdirp = require('mkdirp');
//const Map = require("collections/map");
const once = require('lodash.once');

const station = "36";

var tempBuffer = Buffer.alloc(0);
var metaInt = 1000000000;
var toDownloadSet = new Set();
var toDownload = [];


const oldLog = console.log;
console.log = (...x) => oldLog((new Date()).toJSON(), ...x)

// https://stackoverflow.com/questions/44050266/get-info-from-streaming-radio

// https://open.fm/cover/%(width)x%(height)/%(id)
const getTrackKey = ({song: {artist, title}}) => `${artist} - ${title}`.replace('/', '');
const getTrackAlbum = ({ song: {artist, album}}) => `${artist} - ${album.title} [${album.year}]`;

const getNewList = () => {
  request(`https://open.fm/api/api-ext/v2/channels/${station}/medium.json`, function(error, response, body) {
    if(error) { console.log(`error: ${error}`) }
    if(response && response.statusCode != 200) { 
      console.log(`Song list returned http code ${response.statusCode}!`);
      return; 
    }
    //console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
    var {tracks} = JSON.parse(body);

    tracks.forEach(track => {
      if(lastGGTime > track.end) return;

      let albumName = getTrackAlbum(track);
      let albumPath = path.join('out', station, albumName);


      mkdirp(albumPath, function(err) {
        if(err) { 
          console.log("Error creating album path!"); 
          return;
        }

        let coverPath = path.join(albumPath, 'cover.jpg');
        fs.access(coverPath, fs.constants.F_OK, (err) => {
          if(!err) return; // file already exists - do nothing
          request(`http://open.fm/cover/1400x1400/${track.song.album.cover_uri}`)
            .pipe(fs.createWriteStream(coverPath))
            .on("finish", () => {console.log(`cover for ${albumName} downloaded`)});


        });
      
        let trackKey = getTrackKey(track); 
        let songPath = path.join(albumPath, `${trackKey}.m4a`);
        fs.access(songPath, fs.constants.F_OK, (err) => {
          if(!err) return; // file already exists - do nothing
          track.songPath = songPath;
          if(!toDownloadSet.has(trackKey)) {
            toDownloadSet.add(trackKey); 

            toDownload.push(track);
            toDownload = toDownload.sort((a,b) => a.begin > b.begin);
          }
        });
      });
    });
  });
  //console.log(toDownload.slice(0,3));
};

var firstGetList = once(getNewList);
setInterval(getNewList, 1000*60*3); // 3 min




let lastGGTime = 0;
let ommitedMeta = 0;
request
//.get(`http://stream.open.fm/${station}`) 
  .get({ url: `http://stream.open.fm/${station}`, headers: { 'Icy-Metadata': 1 }})
  .on('response', function(response) {
  //  console.log(response.statusCode) // 200
  //  console.log(response.headers) // 'image/png'
    metaInt = +response.headers['icy-metaint'];
  //  console.log(metaInt); 
  })
  .on('data', (chunk) => {
    //console.log(`${tempBuffer.length} -> ${tempBuffer.length + chunk.length}`); 
    tempBuffer = Buffer.concat([tempBuffer, chunk]);
   
    let nextMeta = metaInt * (ommitedMeta + 1 );
    if(tempBuffer.length > nextMeta + 256) { // 256 - safe buffer to make sure that full meta received in chunk MAYBE: what if chunk >> metaInt ? :>
      let metaEnd = nextMeta + 1 + tempBuffer.readUInt8(nextMeta)*16;
      let metaString = tempBuffer.toString("utf8", nextMeta + 1, metaEnd);

      tempBuffer = Buffer.concat([tempBuffer.slice(0, nextMeta), tempBuffer.slice(metaEnd)]); // remove meta from tempBuffer;

      if(metaString.length) {
        // console.log("metaString:",metaString);
        let GGTime = metaString.match(/GGTime='([\d.]+)'/)[1];
        // console.log("GGTime:", GGTime);
       

        toDownload.slice(0,3).forEach((track) => {
          let startPoint =  Math.max(
                              Math.floor(
                                ((track.begin - lastGGTime) / (GGTime - lastGGTime)) * nextMeta
                              ), 0
                            );


          let endPoint =  Math.min(
                              Math.ceil(
                                ((track.end - lastGGTime) / (GGTime - lastGGTime)) * nextMeta
                              ), nextMeta
                            );


          if(!track.file) {
            if(track.begin < lastGGTime) {
              console.log(`ommitting ${getTrackKey(track)}`);
              toDownloadSet.delete(getTrackKey(track));
              toDownload.shift();
              return;
            }

            if(track.begin > GGTime) {
                return;
            }

            console.log(`Creating ${getTrackKey(track)}`);
            track.file = fs.createWriteStream(track.songPath);
          }

          track.file.write(tempBuffer.slice(startPoint, endPoint));

          if(endPoint < nextMeta) {
            console.log(`Saving ${track.songPath}`);
            track.file.end();
            toDownload.shift();
          }
        });
        
        /*
        if(GGTime > toDownload[0].end) {
          // some song ended..
          var act = toDownload.shift();


        }
        */
        lastGGTime = GGTime;
        firstGetList();
        tempBuffer = tempBuffer.slice(nextMeta);
        ommitedMeta = 0;
      } else {
        // console.log("metaomitted");
        ommitedMeta++;
      }
      //chunk = Buffer.from(tempBuffer, 0, metaInt-1);

      //actFile && actFile.write(chunk);

    }
  })
  .on('end', () => {
    
    // actFile && actFile.end();
  });
  //.pipe(fs.createWriteStream("asdasd" + '.mp4'))

